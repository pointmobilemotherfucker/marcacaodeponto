package marcacaodeponto.fruki.com.br.marcaodeponto.model;

import java.io.Serializable;
import java.util.List;

public class Usuario implements Serializable {

    private static final long serialVersionUID = 7128365543173584976L;
    private int id;
    private String nome;
    private int matricula;
    private String senha;
    private List<Local> locais;
    private List<Marcacao> marcacoes;

    public Usuario() {
    }

    public Usuario(int id, String nome, int matricula, String senha) {
        this.id = id;
        this.matricula = matricula;
        this.senha = senha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public List<Local> getLocais() {
        return locais;
    }

    public void setLocais(List<Local> locais) {
        this.locais = locais;
    }

    public List<Marcacao> getMarcacoes() {
        return marcacoes;
    }

    public void setMarcacoes(List<Marcacao> marcacoes) {
        this.marcacoes = marcacoes;
    }
}
