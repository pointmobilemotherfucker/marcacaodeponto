package marcacaodeponto.fruki.com.br.marcaodeponto.control;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import marcacaodeponto.fruki.com.br.marcaodeponto.R;
import marcacaodeponto.fruki.com.br.marcaodeponto.model.Marcacao;

public class MarcacaoAdapter extends BaseAdapter {

    private Context ctx;
    private List<Marcacao> marcacoes;

    public MarcacaoAdapter(Context ctx, List<Marcacao> marcacoes) {
        this.ctx = ctx;
        this.marcacoes = marcacoes;
    }

    @Override
    public int getCount() {
        return marcacoes.size();
    }

    @Override
    public Object getItem(int position) {
        return marcacoes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return marcacoes.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Marcacao marcacao = marcacoes.get(position);
        LayoutInflater inflanter = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflanter.inflate(R.layout.listview_horarios, null);
        TextView txvHorario = (TextView) view.findViewById(R.id.txvHorario);
        if (marcacao.getHora().contains(".")) {
            String[] horario = marcacao.getHora().replace(".", ",").split(",");
            String hora = horario[0];
            String minuto = horario[1];
            if (hora.length() < 2){
                hora = "0" + hora;
            }
            if(minuto.length() < 2){
                minuto = "0" + minuto;
            }
            txvHorario.setText(hora + ":" + minuto);
        } else {
            if (marcacao.getHora().length() > 1) {
                txvHorario.setText(marcacao.getHora() + ":00");
            } else {
                txvHorario.setText("0" + marcacao.getHora() + ":00");
            }
        }
        if (marcacao.getSinc()) {
            txvHorario.setTextColor(ctx.getResources().getColor(R.color.Azul));
        } else {
            txvHorario.setTextColor(ctx.getResources().getColor(R.color.Vermelho));
        }
        return view;
    }
}
