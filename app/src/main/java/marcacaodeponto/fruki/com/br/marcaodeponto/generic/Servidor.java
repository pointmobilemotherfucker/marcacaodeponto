package marcacaodeponto.fruki.com.br.marcaodeponto.generic;

import android.content.Context;

import java.util.List;

public class Servidor {

    private Context ctx;
    private String retorno;
    private String URLWebService;

    public Servidor(Context ctx) {
        this.ctx = ctx;
    }

    public String comunicar(String service, String webService, List<ParametrosSOAP> listaParametros) {
        Servidor server = new Servidor(ctx);
        String local = "http://glassfish.fruki.com.br:8080/MarcacaoDePonto";
        URLWebService = local + "/" + service;
        ExecutarServico wService = new ExecutarServico(service, webService, "http://glassfish.fruki.com.br/", URLWebService, listaParametros);
        retorno = wService.executar().toString();
        return retorno;
    }
}
