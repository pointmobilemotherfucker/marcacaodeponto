package marcacaodeponto.fruki.com.br.marcaodeponto.control;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import marcacaodeponto.fruki.com.br.marcaodeponto.generic.DatabaseManager;
import marcacaodeponto.fruki.com.br.marcaodeponto.model.Local;

public class LocalControl {

    private SQLiteDatabase db;
    private DatabaseManager criarBanco;
    private Context ctx;

    public LocalControl(Context ctx) {
        this.ctx = ctx;
        criarBanco = new DatabaseManager(ctx);
    }

    public long inserir(Local local) {
        long retorno = 0;
        db = criarBanco.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", local.getId());
        values.put("id_usuario", local.getIdUsuario());
        values.put("lat", local.getLat());
        values.put("long", local.getLon());
        retorno = db.insert("local", null, values);
        Log.i(LocalControl.class.getName(), "Local Inserido");
        db.close();
        return retorno;
    }

    public long inserirLista(List<Local> locais) {
        long retorno = 0;
        db = criarBanco.getWritableDatabase();
        for (Local local : locais) {
            ContentValues values = new ContentValues();
            values.put("id", local.getId());
            values.put("id_usuario", local.getIdUsuario());
            values.put("lat", local.getLat());
            values.put("long", local.getLon());
            retorno = db.insert("local", null, values);
            if (retorno == 0) {
                break;
            }
        }
        db.close();
        return retorno;
    }

    public List<Local> buscarLocaisByIdUsuario(int idUsuario){
        List<Local> retorno = new ArrayList<>();
        db = criarBanco.getReadableDatabase();
        Cursor cursor = db.query("local", new String[] {"id", "id_usuario", "lat", "long"}, "id_usuario = " + idUsuario, null, null, null,null);
        while(cursor.moveToNext()){
            Local local = new Local();
            local.setId(cursor.getInt(cursor.getColumnIndex("id")));
            local.setIdUsuario(cursor.getInt(cursor.getColumnIndex("id_usuario")));
            local.setLat(cursor.getString(cursor.getColumnIndex("lat")));
            local.setLon(cursor.getString(cursor.getColumnIndex("long")));
            retorno.add(local);
        }
        cursor.close();
        db.close();
        return retorno;
    }

    public long deletar(Local local) {
        long retorno = 0;
        db = criarBanco.getWritableDatabase();
        db.delete("local", "id = " + local.getId(), null);
        return retorno;
    }

    public long deletarByIdUsuario(int idUsuario) {
        long retorno = 0;
        db = criarBanco.getWritableDatabase();
        db.delete("local", "id_usuario = " + idUsuario, null);
        return retorno;
    }
}
