package marcacaodeponto.fruki.com.br.marcaodeponto.view;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Calendar;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import marcacaodeponto.fruki.com.br.marcaodeponto.R;
import marcacaodeponto.fruki.com.br.marcaodeponto.control.LocalControl;
import marcacaodeponto.fruki.com.br.marcaodeponto.control.MarcacaoAdapter;
import marcacaodeponto.fruki.com.br.marcaodeponto.control.MarcacaoControl;
import marcacaodeponto.fruki.com.br.marcaodeponto.control.UsuarioControl;
import marcacaodeponto.fruki.com.br.marcaodeponto.generic.FingerprintHandler;
import marcacaodeponto.fruki.com.br.marcaodeponto.generic.Util;
import marcacaodeponto.fruki.com.br.marcaodeponto.model.Local;
import marcacaodeponto.fruki.com.br.marcaodeponto.model.Marcacao;
import marcacaodeponto.fruki.com.br.marcaodeponto.model.Usuario;

public class Marcacoes extends AppCompatActivity {

    private Context ctx;
    private Usuario usuario;
    private EditText edtData;
    private ListView ltvMarcacoes;
    private Calendar myCalendar;
    private DatePickerDialog.OnDateSetListener date;
    private ProgressDialog dialogEnviar;
    private KeyStore keyStore;
    private Cipher cipher;
    private static final String KEY_NAME = "androidHive";
    private KeyguardManager keyguardManager;
    private FingerprintManager fingerprintManager;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marcacoes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);

        ctx = this;
        myCalendar = Calendar.getInstance();
        ltvMarcacoes = (ListView) findViewById(R.id.ltvMarcacoes);
        edtData = (EditText) findViewById(R.id.edtData);

        keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

        InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        im.showSoftInput(edtData, 0);

        UsuarioControl uControl = new UsuarioControl(ctx);
        usuario = uControl.buscar();

        edtData.setText(Util.hojeString());

        MarcacaoAdapter mAdapter = new MarcacaoAdapter(ctx, Util.buscarMarcaçoesByData(usuario.getMarcacoes(), edtData.getText().toString()));
        ltvMarcacoes.setAdapter(mAdapter);

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                edtData.setText(Util.formatarDataCalendar(myCalendar));
                MarcacaoAdapter mAdapter = new MarcacaoAdapter(ctx, Util.buscarMarcaçoesByData(usuario.getMarcacoes(), edtData.getText().toString()));
                ltvMarcacoes.setAdapter(mAdapter);

            }
        };

        edtData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(ctx, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                boolean locais = false;
                if(!fingerprintManager.isHardwareDetected()){
                    if (usuario.getLocais().size() > 0) {
                        locais = true;
                        Log.i(Marcacoes.class.getName(), "Possui Locais");
                    }

                    if (!locais || Util.localPermitido(usuario.getLocais(), Double.parseDouble(Util.pegarLatitude(ctx)), Double.parseDouble(Util.pegarLongitude(ctx)))) {
                        MarcacaoControl mControl = new MarcacaoControl(ctx);
                        Marcacao marcacao = new Marcacao();
                        marcacao.setId(mControl.proximoId());
                        marcacao.setIdUsuario(usuario.getId());
                        marcacao.setData(Util.hojeString());
                        marcacao.setHora(Util.horaString());
                        marcacao.setLat(Util.pegarLatitude(ctx));
                        marcacao.setLon(Util.pegarLongitude(ctx));
                        marcacao.setSinc(false);
                        if (!mControl.existe(marcacao)) {
                            long retorno = mControl.inserir(marcacao);
                            String conectando = getString(R.string.conectando);
                            String tentandoConexao = getString(R.string.tentando_conexao_aguarde);
                            dialogEnviar = ProgressDialog.show(ctx, conectando, tentandoConexao, true);
                            Thread tEnviar = new Thread("Thread Logar") {
                                public @Override
                                void run() {
                                    UsuarioControl uControl = new UsuarioControl(ctx);
                                    usuario = uControl.buscar();
                                    List<Marcacao> marcacoesNaoEnviadas = Util.marcacoesNaoEnviadas(usuario.getMarcacoes());
                                    MarcacaoControl mControl = new MarcacaoControl(ctx);
                                    int retorno = mControl.enviar(marcacoesNaoEnviadas, ctx);
                                    enviar.sendEmptyMessage(retorno);
                                }
                            };
                            tEnviar.start();
                        } else {
                            Toast.makeText(ctx, "Já existe uma marcação para este horário. Aguarde um momente e tente novmente.", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(ctx, "Local não permitido para marcação de ponto.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.i(Marcacoes.class.getName(), "Teste");
                    // Checks whether fingerprint permission is set on manifest
                    if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(ctx, "Fingerprint authentication permission not enabled", Toast.LENGTH_LONG).show();
                    }else{
                        // Check whether at least one fingerprint is registered
                        if (!fingerprintManager.hasEnrolledFingerprints()) {
                            Toast.makeText(ctx, "Register at least one fingerprint in Settings", Toast.LENGTH_LONG).show();
                        }else{
                            // Checks whether lock screen security is enabled or not
                            if (!keyguardManager.isKeyguardSecure()) {
                                Toast.makeText(ctx, "Lock screen security not enabled in Settings", Toast.LENGTH_LONG).show();
                            }else{
                                generateKey();

                                if (cipherInit()) {
                                    FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                                    FingerprintHandler helper = new FingerprintHandler(ctx);
                                    helper.startAuth(fingerprintManager, cryptoObject);
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exit:
                UsuarioControl usuarioControl = new UsuarioControl(ctx);
                usuarioControl.deletar(usuario);
                for (Marcacao marcacao : usuario.getMarcacoes()) {
                    MarcacaoControl marcacaoControl = new MarcacaoControl(ctx);
                    marcacaoControl.deletar(marcacao);
                }
                for (Local local : usuario.getLocais()) {
                    LocalControl localControl = new LocalControl(ctx);
                    localControl.deletar(local);
                }
                finish();
                return (true);
        }
        return (super.onOptionsItemSelected(item));
    }

    private Handler enviar = new Handler() {
        public void handleMessage(Message msg) {
            dialogEnviar.dismiss();
            String mensagem = "";
            UsuarioControl uControl = new UsuarioControl(ctx);
            MarcacaoAdapter mAdapter;
            switch (msg.what) {
                case 1: //Enviado
                    mensagem = getString(R.string.marcacao_enviada);
                    Toast.makeText(ctx, mensagem, Toast.LENGTH_LONG).show();
                    usuario = uControl.buscar();
                    mAdapter = new MarcacaoAdapter(ctx, Util.buscarMarcaçoesByData(usuario.getMarcacoes(), edtData.getText().toString()));
                    ltvMarcacoes.setAdapter(mAdapter);
                    break;
                case 2: //Sem internet
                    mensagem = getString(R.string.sem_conexao);
                    Toast.makeText(ctx, mensagem, Toast.LENGTH_LONG).show();
                    break;
                default:
                    break;
            }
        }
    };
}