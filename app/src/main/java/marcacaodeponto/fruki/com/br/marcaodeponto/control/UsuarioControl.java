package marcacaodeponto.fruki.com.br.marcaodeponto.control;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import marcacaodeponto.fruki.com.br.marcaodeponto.generic.Compactador;
import marcacaodeponto.fruki.com.br.marcaodeponto.generic.DatabaseManager;
import marcacaodeponto.fruki.com.br.marcaodeponto.generic.ParametrosSOAP;
import marcacaodeponto.fruki.com.br.marcaodeponto.generic.Servidor;
import marcacaodeponto.fruki.com.br.marcaodeponto.generic.Util;
import marcacaodeponto.fruki.com.br.marcaodeponto.model.Local;
import marcacaodeponto.fruki.com.br.marcaodeponto.model.Marcacao;
import marcacaodeponto.fruki.com.br.marcaodeponto.model.Usuario;

public class UsuarioControl {

    private SQLiteDatabase db;
    private DatabaseManager criarBanco;
    private Context ctx;

    public UsuarioControl(Context ctx) {
        this.ctx = ctx;
        criarBanco = new DatabaseManager(ctx);
    }

    public long inserir(Usuario usuario) {
        long retorno = 0;
        db = criarBanco.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", usuario.getId());
        values.put("matricula", usuario.getMatricula());
        values.put("senha", usuario.getSenha());
        values.put("nome", usuario.getNome());
        retorno = db.insert("usuario", null, values);
        db.close();
        return retorno;
    }

    public long alterar(Usuario usuario) {
        long retorno = 0;
        db = criarBanco.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", usuario.getId());
        values.put("matricula", usuario.getMatricula());
        values.put("senha", usuario.getSenha());
        values.put("nome", usuario.getNome());
        retorno = db.update("usuario", values, "id = " + usuario.getId(), null);
        db.close();

        return retorno;
    }

    public Usuario buscar() {
        Usuario retorno = null;
        db = criarBanco.getReadableDatabase();
        Cursor cursor = db.query("usuario", new String[]{"id", "matricula", "senha", "nome"}, null, null, null, null, null);
        while (cursor.moveToNext()) {
            retorno = new Usuario();
            retorno.setId(cursor.getInt(cursor.getColumnIndex("id")));
            retorno.setMatricula(cursor.getInt(cursor.getColumnIndex("matricula")));
            retorno.setSenha(cursor.getString(cursor.getColumnIndex("senha")));
            retorno.setNome(cursor.getString(cursor.getColumnIndex("nome")));
        }
        cursor.close();
        db.close();

        if (retorno != null) {
            MarcacaoControl mControl = new MarcacaoControl(ctx);
            retorno.setMarcacoes(mControl.buscarByIdUsuario(retorno.getId()));
            LocalControl lControl = new LocalControl(ctx);
            retorno.setLocais(lControl.buscarLocaisByIdUsuario(retorno.getId()));
        }

        return retorno;
    }

    public long deletar(Usuario usuario) {
        long retorno = 0;
        db = criarBanco.getWritableDatabase();
        db.delete("usuario", "id = " + usuario.getId(), null);
        return retorno;
    }

    public int entrar(Usuario usuario) {
        int retorno = 5;
        db = criarBanco.getReadableDatabase();
        String[] colunas = {"id", "matricula", "senha"};
        Cursor cursor = db.query("usuario", colunas, "matricula = " + usuario.getMatricula() + " AND senha = '" + usuario.getSenha() + "'", null, null, null, null);
        if (cursor.moveToFirst()) {
            retorno = 0;
        } else {
            if (Util.estaConectado(ctx)) {
                List<ParametrosSOAP> parametros = new ArrayList<>();
                String matricula = String.valueOf(usuario.getMatricula());
                ParametrosSOAP p1 = new ParametrosSOAP("matricula", matricula);
                ParametrosSOAP p2 = new ParametrosSOAP("senha", usuario.getSenha());
                parametros.add(p1);
                parametros.add(p2);
                Servidor servidor = new Servidor(ctx);
                String resposta = servidor.comunicar("LoginService", "login" +
                        "", parametros);
                Compactador compactador = new Compactador();
                Usuario usuarioRetorno = (Usuario) compactador.deSerializeObject(resposta);
                if (usuarioRetorno != null) {
                    inserir(usuarioRetorno);
                    MarcacaoControl mControl = new MarcacaoControl(ctx);
                    mControl.inserirLista(usuarioRetorno.getMarcacoes());
                    LocalControl lcontrol = new LocalControl(ctx);
                    lcontrol.inserirLista(usuarioRetorno.getLocais());
                    retorno = 0;
                } else {
                    retorno = 1;
                }
            } else {
                retorno = 2;
            }
        }
        cursor.close();
        db.close();

        return retorno;
    }
}