package marcacaodeponto.fruki.com.br.marcaodeponto.model;

import java.io.Serializable;

/**
 *
 * @author LUCAS_SCHNEIDER
 */
public class Local implements Serializable{

    private static final long serialVersionUID = 6710085402027094460L;
    private int id;
    private int idUsuario;
    private String lon;
    private String lat;

    public Local() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }
}
