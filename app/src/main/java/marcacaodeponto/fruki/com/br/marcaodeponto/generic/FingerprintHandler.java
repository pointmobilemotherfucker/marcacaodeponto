package marcacaodeponto.fruki.com.br.marcaodeponto.generic;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import marcacaodeponto.fruki.com.br.marcaodeponto.R;
import marcacaodeponto.fruki.com.br.marcaodeponto.control.MarcacaoAdapter;
import marcacaodeponto.fruki.com.br.marcaodeponto.control.MarcacaoControl;
import marcacaodeponto.fruki.com.br.marcaodeponto.control.UsuarioControl;
import marcacaodeponto.fruki.com.br.marcaodeponto.model.Marcacao;
import marcacaodeponto.fruki.com.br.marcaodeponto.model.Usuario;
import marcacaodeponto.fruki.com.br.marcaodeponto.view.Marcacoes;

@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    private Context ctx;
    private AlertDialog alerta;
    private ProgressDialog dialogEnviar;
    private Usuario usuario;

    // Constructor
    public FingerprintHandler(Context mContext) {
        ctx = mContext;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        CancellationSignal cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        AlertDialog.Builder alertaBuilder = new AlertDialog.Builder(ctx);
        alertaBuilder.setTitle("Marcação");
        alertaBuilder.setMessage("Posicione o seu dedo sobre a o sensor para registrar uma nova marcação.");
        alertaBuilder.setNeutralButton("Cancelar", null);
        alerta = alertaBuilder.show();
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        Log.i(FingerprintHandler.class.getName(), "Erro");
        Toast.makeText(ctx, "Dedo não identificado no cadastro. Tente novamente.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        alerta.setMessage("O dedo foi retirado rápido demais. Tente novamente.");
        Log.i(FingerprintHandler.class.getName(), "Help");
    }

    @Override
    public void onAuthenticationFailed() {
        alerta.dismiss();
        Toast.makeText(ctx, "Dedo não identificado no cadastro. Tente novamente.", Toast.LENGTH_LONG).show();
        Log.i(FingerprintHandler.class.getName(), "Falha");
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        alerta.dismiss();
        update();
        Log.i(FingerprintHandler.class.getName(), "Sucesso");
    }

    private void update() {
        final UsuarioControl uControl = new UsuarioControl(ctx);
        usuario = uControl.buscar();
        boolean locais = false;
        if (usuario.getLocais().size() > 0) {
            locais = true;
            Log.i(Marcacoes.class.getName(), "Possui Locais");
        }

        if (!locais || Util.localPermitido(usuario.getLocais(), Double.parseDouble(Util.pegarLatitude(ctx)), Double.parseDouble(Util.pegarLongitude(ctx)))) {
            MarcacaoControl mControl = new MarcacaoControl(ctx);
            Marcacao marcacao = new Marcacao();
            marcacao.setId(mControl.proximoId());
            marcacao.setIdUsuario(usuario.getId());
            marcacao.setData(Util.hojeString());
            marcacao.setHora(Util.horaString());
            marcacao.setLat(Util.pegarLatitude(ctx));
            marcacao.setLon(Util.pegarLongitude(ctx));
            marcacao.setSinc(false);
            if (!mControl.existe(marcacao)) {
                long retorno = mControl.inserir(marcacao);
                String conectando = ctx.getString(R.string.conectando);
                String tentandoConexao = ctx.getString(R.string.tentando_conexao_aguarde);
                dialogEnviar = ProgressDialog.show(ctx, conectando, tentandoConexao, true);
                Thread tEnviar = new Thread("Thread Logar") {
                    public @Override
                    void run() {
                        usuario = uControl.buscar();
                        List<Marcacao> marcacoesNaoEnviadas = Util.marcacoesNaoEnviadas(usuario.getMarcacoes());
                        MarcacaoControl mControl = new MarcacaoControl(ctx);
                        int retorno = mControl.enviar(marcacoesNaoEnviadas, ctx);
                        enviar.sendEmptyMessage(retorno);
                    }
                };
                tEnviar.start();
            } else {
                Toast.makeText(ctx, "Já existe uma marcação para este horário. Aguarde um momente e tente novmente.", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(ctx, "Local não permitido para marcação de ponto.", Toast.LENGTH_LONG).show();
        }
    }

    private Handler enviar = new Handler() {
        public void handleMessage(Message msg) {
            dialogEnviar.dismiss();
            String mensagem = "";
            UsuarioControl uControl = new UsuarioControl(ctx);
            TextView edtData;
            ListView ltvMarcacoes;
            MarcacaoAdapter mAdapter;
            Log.i(FingerprintHandler.class.getName(), msg.what + "");
            switch (msg.what) {
                case 1: //Enviado
                    mensagem = ctx.getString(R.string.marcacao_enviada);
                    Toast.makeText(ctx, mensagem, Toast.LENGTH_LONG).show();
                    usuario = uControl.buscar();
                    edtData = (TextView) ((Activity) ctx).findViewById(R.id.edtData);
                    ltvMarcacoes = (ListView) ((Activity) ctx).findViewById(R.id.ltvMarcacoes);
                    mAdapter = new MarcacaoAdapter(ctx, Util.buscarMarcaçoesByData(usuario.getMarcacoes(), edtData.getText().toString()));
                    ltvMarcacoes.setAdapter(mAdapter);
                    break;
                case 2: //Sem internet
                    mensagem = ctx.getString(R.string.sem_conexao);
                    Toast.makeText(ctx, mensagem, Toast.LENGTH_LONG).show();
                    usuario = uControl.buscar();
                    edtData = (TextView) ((Activity) ctx).findViewById(R.id.edtData);
                    ltvMarcacoes = (ListView) ((Activity) ctx).findViewById(R.id.ltvMarcacoes);
                    mAdapter = new MarcacaoAdapter(ctx, Util.buscarMarcaçoesByData(usuario.getMarcacoes(), edtData.getText().toString()));
                    ltvMarcacoes.setAdapter(mAdapter);
                    break;
                default:
                    break;
            }
        }
    };

}
