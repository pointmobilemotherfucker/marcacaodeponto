package marcacaodeponto.fruki.com.br.marcaodeponto.generic;

public class ParametrosSOAP {

        private String codParametro;
        private String valorParametro;

        public ParametrosSOAP(String codParametro, String valorParametro){
            this.codParametro = codParametro;
            this.valorParametro = valorParametro;
        }
        public String getCodParametro() {
            return codParametro;
        }
        public void setCodParametro(String codParametro) {
            this.codParametro = codParametro;
        }
        public String getValorParametro() {
            return valorParametro;
        }
        public void setValorParametro(String valorParametro) {
            this.valorParametro = valorParametro;
        }
}
