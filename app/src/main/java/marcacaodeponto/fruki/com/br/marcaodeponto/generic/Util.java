package marcacaodeponto.fruki.com.br.marcaodeponto.generic;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import marcacaodeponto.fruki.com.br.marcaodeponto.R;
import marcacaodeponto.fruki.com.br.marcaodeponto.control.MarcacaoAdapter;
import marcacaodeponto.fruki.com.br.marcaodeponto.model.Local;
import marcacaodeponto.fruki.com.br.marcaodeponto.model.Marcacao;

public class Util {

    public static boolean estaConectado(Context ctx) {
        boolean retorno = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected()) {
                retorno = true;
            } else if (cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected()) {
                retorno = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            retorno = false;
        }
        return retorno;
    }

    public static String formataData(String data) {
        String retorno = "";
        String[] dataSplit = data.split("/");
        retorno = dataSplit[1] + "/" + dataSplit[0];
        dataSplit = dataSplit[2].split(" ");
        retorno = dataSplit[0] + retorno;
        return retorno;
    }

    public static String formatarDataCalendar(Calendar data) {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        return sdf.format(data.getTime());
    }

    public static String formataDataBanco(String data) {
        String retorno = "";
        String[] dataSplit = data.split("/");
        retorno = dataSplit[2] + "-" + dataSplit[1] + "-" + dataSplit[0] + " 00:00:00.0";
        return retorno;
    }

    public static String formataHoraBanco(String hora) {
        String retorno = "";
        String[] horaSplit = hora.split(":");
        retorno = horaSplit[0] + "." + horaSplit[1];
        return retorno;
    }

    public static String hojeString() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String horaString() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int horas = cal.get(Calendar.HOUR_OF_DAY);
        int minutos = cal.get(Calendar.MINUTE);
        String horario = String.valueOf(horas + ":" + minutos);
        return horario;
    }

    private static double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(Calcular.deg2rad(lat1)) * Math.sin(Calcular.deg2rad(lat2)) + Math.cos(Calcular.deg2rad(lat1)) * Math.cos(Calcular.deg2rad(lat2)) * Math.cos(Calcular.deg2rad(theta));
        dist = Math.acos(dist);
        dist = Calcular.rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;
        Log.i(Util.class.getName(), dist + "");
        return (dist);
    }

    public static boolean localPermitido(List<Local> locais, double lat, double lon){
        boolean retorno = false;
        for(Local local : locais){
            double distancia = distance(lat, lon, Double.parseDouble(local.getLat()), Double.parseDouble(local.getLon()));
            if (distancia < 0.500 && distancia > -0.500){
                Log.i(Util.class.getName(), "Local Permitido");
                return true;
            }
        }
        return retorno;
    }



    public static List<Marcacao> buscarMarcaçoesByData(List<Marcacao> marcacoes, String data) {
        List<Marcacao> retorno = new ArrayList<>();
        for (Marcacao marcacao : marcacoes) {
            if (marcacao.getData().equals(formataDataBanco(data))) {
                retorno.add(marcacao);
            }
        }
        return retorno;
    }

    public static String pegarLatitude(Context ctx) {
        String retorno = "";
        Geocoder geocoder;
        String bestProvider;
        List<Address> user = null;
        double lat;
        double lng;

        LocationManager lm = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        bestProvider = lm.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(ctx, ctx.getString(R.string.permissao_localizacao), Toast.LENGTH_LONG).show();
        } else {
            Location location = lm.getLastKnownLocation(bestProvider);
            if (location == null) {
                Toast.makeText(ctx, ctx.getString(R.string.localizacao_nao_encontrada), Toast.LENGTH_LONG).show();
            } else {
                geocoder = new Geocoder(ctx);
                try {
                    user = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    lat = (double) user.get(0).getLatitude();
                    retorno = String.valueOf(lat);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return retorno;
    }

    public static String pegarLongitude(Context ctx) {
        String retorno = "";
        Geocoder geocoder;
        String bestProvider;
        List<Address> user = null;
        double lat;
        double lng;

        LocationManager lm = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        bestProvider = lm.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(ctx, ctx.getString(R.string.permissao_localizacao), Toast.LENGTH_LONG).show();
        } else {
            Location location = lm.getLastKnownLocation(bestProvider);
            if (location == null) {
                Toast.makeText(ctx, ctx.getString(R.string.localizacao_nao_encontrada), Toast.LENGTH_LONG).show();
            } else {
                geocoder = new Geocoder(ctx);
                try {
                    user = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    lng = (double) user.get(0).getLongitude();
                    retorno = String.valueOf(lng);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return retorno;
    }

    public static List<Marcacao> marcacoesNaoEnviadas(List<Marcacao> marcacoes){
        List<Marcacao> retorno = new ArrayList<Marcacao>();
        for (Marcacao marcacao : marcacoes){
            if(!marcacao.getSinc()){
                retorno.add(marcacao);
            }
        }
        return retorno;
    }
}
