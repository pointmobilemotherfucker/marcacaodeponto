package marcacaodeponto.fruki.com.br.marcaodeponto.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import marcacaodeponto.fruki.com.br.marcaodeponto.R;
import marcacaodeponto.fruki.com.br.marcaodeponto.control.LocalControl;
import marcacaodeponto.fruki.com.br.marcaodeponto.control.MarcacaoControl;
import marcacaodeponto.fruki.com.br.marcaodeponto.control.UsuarioControl;
import marcacaodeponto.fruki.com.br.marcaodeponto.model.Usuario;

public class Login extends AppCompatActivity {

    private Context ctx;
    private EditText edtMatricula;
    private EditText edtSenha;
    private Button btnEntrar;
    private ProgressDialog dialogLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ctx = this;

        edtMatricula = (EditText) findViewById(R.id.edtMatricula);
        edtSenha = (EditText) findViewById(R.id.edtSenha);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);

        UsuarioControl usuarioControl = new UsuarioControl(ctx);
        Usuario usuario = usuarioControl.buscar();
        if (usuario != null) {
            int retorno = usuarioControl.entrar(usuario);
            Intent intent = new Intent(ctx, Marcacoes.class);
            ctx.startActivity(intent);
            finish();
        }

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sMatricula = edtMatricula.getText().toString();
                String sSenha = edtSenha.getText().toString();
                if (sMatricula.length() > 0 && sSenha.length() > 0) {
                    String conectando = getString(R.string.conectando);
                    String tentandoConexao = getString(R.string.tentando_conexao_aguarde);
                    dialogLogin = ProgressDialog.show(ctx, conectando, tentandoConexao, true);
                    Thread tLogar = new Thread("Thread Logar") {
                        public @Override
                        void run() {
                            Looper.prepare();
                            String sMatricula = edtMatricula.getText().toString();
                            String sSenha = edtSenha.getText().toString();
                            Usuario usuario = new Usuario();
                            int iMatricula = Integer.parseInt(sMatricula);
                            usuario.setMatricula(iMatricula);
                            usuario.setSenha(sSenha);
                            UsuarioControl cUsuario = new UsuarioControl(ctx);
                            int retorno = cUsuario.entrar(usuario);
                            logar.sendEmptyMessage(retorno);
                        }
                    };
                    tLogar.start();
                } else {
                    String mensagem = getString(R.string.usuario_senha_incorreto);
                    Toast.makeText(ctx, mensagem, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private Handler logar = new Handler() {
        public void handleMessage(Message msg) {
            dialogLogin.dismiss();
            String mensagem = "";
            switch (msg.what) {
                case 0:
                    Intent intent = new Intent(ctx, Marcacoes.class);
                    ctx.startActivity(intent);
                    finish();
                    break;
                case 1:
                    mensagem = getString(R.string.usuario_senha_incorreto);
                    Toast.makeText(ctx, mensagem, Toast.LENGTH_LONG).show();
                    break;
                case 2:
                    mensagem = getString(R.string.sem_conexao);
                    Toast.makeText(ctx, mensagem, Toast.LENGTH_LONG).show();
                    break;
                default:
                    break;
            }
        }
    };
}