package marcacaodeponto.fruki.com.br.marcaodeponto.generic;

import android.os.StrictMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


public class ExecutarServico {

    private String SOAP_ACTION;
    private String METHOD_NAME;
    private String NAMESPACE;
    private String URL;
    private List<ParametrosSOAP> listaParametros = new ArrayList<ParametrosSOAP>();

    public ExecutarServico(String SOAP_ACTION, String METHOD_NAME, String NAMESPACE, String URL, List<ParametrosSOAP> listaParametros) {
        this.SOAP_ACTION = SOAP_ACTION;
        this.METHOD_NAME = METHOD_NAME;
        this.NAMESPACE = NAMESPACE;
        this.URL = URL;
        this.listaParametros = listaParametros;
    }

    public Object executar() {
        Object results = new Object();
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        for (Iterator<ParametrosSOAP> its = listaParametros.iterator(); its.hasNext();) {
            ParametrosSOAP parametros = its.next();
            request.addProperty(parametros.getCodParametro(), parametros.getValorParametro());
        }
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL, 30000);
        try {
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            androidHttpTransport.call(SOAP_ACTION, envelope);
            results = envelope.getResponse();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        return results;
    }
}
