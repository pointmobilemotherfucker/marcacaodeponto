package marcacaodeponto.fruki.com.br.marcaodeponto.model;

import java.io.Serializable;

/**
 * @author LUCAS_SCHNEIDER
 */
public class Marcacao implements Serializable {

    private static final long serialVersionUID = -5687017849791981853L;
    private int id;
    private int idUsuario;
    private String data;
    private String hora;
    private String lat;
    private String lon;
    private boolean sinc;

    public Marcacao() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public boolean getSinc() {
        return sinc;
    }

    public void setSinc(boolean sinc) {
        this.sinc = sinc;
    }
}
