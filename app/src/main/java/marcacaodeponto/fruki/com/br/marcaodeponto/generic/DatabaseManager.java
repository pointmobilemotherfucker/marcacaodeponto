package marcacaodeponto.fruki.com.br.marcaodeponto.generic;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseManager extends SQLiteOpenHelper {

    private static final String DBNAME = "ponto.db";
    private static final int VERSION = 1;

    public DatabaseManager(Context ctx){
        super(ctx, DBNAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("SQL", "teste");
        String[] sqls = {
                "CREATE TABLE usuario (id INTEGER PRIMARY KEY, matricula INTEGER NOT NULL, senha TEXT NOT NULL, nome TEXT);",
                "CREATE TABLE marcacao (id INTEGER PRIMARY KEY, id_usuario INTEGER NOT NULL, data TEXT, hora TEXT, lat TEXT, long TEXT, sinc INTEGER);",
                "CREATE TABLE local (id INTEGER PRIMARY KEY, id_usuario INTEGER NOT NULL, lat TEXT, long TEXT);",
                "INSERT INTO usuario (id, matricula, senha, nome) VALUES (0, 0,'admin', 'Administrador');"};
                for (String sql :sqls){
                    db.execSQL(sql);
                }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS usuario");
        onCreate(db);
    }
}
