package marcacaodeponto.fruki.com.br.marcaodeponto.control;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import marcacaodeponto.fruki.com.br.marcaodeponto.generic.Compactador;
import marcacaodeponto.fruki.com.br.marcaodeponto.generic.DatabaseManager;
import marcacaodeponto.fruki.com.br.marcaodeponto.generic.ParametrosSOAP;
import marcacaodeponto.fruki.com.br.marcaodeponto.generic.Servidor;
import marcacaodeponto.fruki.com.br.marcaodeponto.generic.Util;
import marcacaodeponto.fruki.com.br.marcaodeponto.model.Marcacao;
import marcacaodeponto.fruki.com.br.marcaodeponto.model.Usuario;

public class MarcacaoControl {

    private SQLiteDatabase db;
    private DatabaseManager criarBanco;
    private Context ctx;

    public MarcacaoControl(Context ctx) {
        this.ctx = ctx;
        criarBanco = new DatabaseManager(ctx);
    }

    public long inserir(Marcacao marcacao) {
        long retorno = 0;
        db = criarBanco.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", marcacao.getId());
        values.put("id_usuario", marcacao.getIdUsuario());
        values.put("data", Util.formataDataBanco(marcacao.getData()));
        values.put("hora", Util.formataHoraBanco(marcacao.getHora()));
        values.put("lat", marcacao.getLat());
        values.put("long", marcacao.getLon());
        boolean sinc = marcacao.getSinc() ? true : false;
        values.put("sinc", sinc);
        retorno = db.insert("marcacao", null, values);
        db.close();
        return retorno;
    }

    public long inserirLista(List<Marcacao> marcacoes) {
        long retorno = 0;
        db = criarBanco.getWritableDatabase();
        for (Marcacao marcacao : marcacoes) {
            ContentValues values = new ContentValues();
            values.put("id", marcacao.getId());
            values.put("id_usuario", marcacao.getIdUsuario());
            values.put("data", marcacao.getData());
            values.put("hora", marcacao.getHora());
            values.put("lat", marcacao.getLat());
            values.put("long", marcacao.getLon());
            values.put("sinc", marcacao.getSinc());
            retorno = db.insert("marcacao", null, values);
            if (retorno == 0) {
                break;
            }
        }
        db.close();
        return retorno;
    }

    public long alterar(Marcacao marcacao) {
        long retorno = 0;
        db = criarBanco.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", marcacao.getId());
        values.put("id_usuario", marcacao.getIdUsuario());
        values.put("data", marcacao.getData());
        values.put("hora", marcacao.getHora());
        values.put("lat", marcacao.getLat());
        values.put("long", marcacao.getLon());
        int sinc = marcacao.getSinc() ? 1 : 0;
        values.put("sinc", sinc);
        retorno = db.update("marcacao", values, "id = ?", new String[]{String.valueOf(marcacao.getId())});
        db.close();
        return retorno;
    }

    public List<Marcacao> buscarByIdUsuario(int idUsuario) {
        List<Marcacao> retorno = new ArrayList<>();
        db = criarBanco.getReadableDatabase();
        Cursor cursor = db.query("marcacao", new String[]{"id", "id_usuario", "data", "hora", "lat", "long", "sinc"}, "id_usuario = " + idUsuario, null, null, null, null);
        while (cursor.moveToNext()) {
            Marcacao marcacao = new Marcacao();
            marcacao.setId(cursor.getInt(cursor.getColumnIndex("id")));
            marcacao.setIdUsuario(cursor.getInt(cursor.getColumnIndex("id_usuario")));
            marcacao.setData(cursor.getString(cursor.getColumnIndex("data")));
            marcacao.setHora(cursor.getString(cursor.getColumnIndex("hora")));
            marcacao.setLat(cursor.getString(cursor.getColumnIndex("lat")));
            marcacao.setLon(cursor.getString(cursor.getColumnIndex("long")));
            boolean sinc = cursor.getInt(cursor.getColumnIndex("sinc")) == 0 ? false : true;
            marcacao.setSinc(sinc);
            retorno.add(marcacao);
        }
        cursor.close();
        db.close();
        return retorno;
    }

    public int proximoId() {
        int retorno = 0;
        db = criarBanco.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT MAX(id) AS id FROM marcacao", null);
        while (cursor.moveToNext()) {
            retorno = cursor.getInt(cursor.getColumnIndex("id")) + 1;
        }
        cursor.close();
        db.close();
        return retorno;
    }

    public long deletar(Marcacao marcacao) {
        long retorno = 0;
        db = criarBanco.getWritableDatabase();
        db.delete("marcacao", "id = " + marcacao.getId(), null);
        return retorno;
    }

    public long deletarByIdUsuario(int idUsuario) {
        long retorno = 0;
        db = criarBanco.getWritableDatabase();
        db.delete("marcacao", "id_usuario = " + idUsuario, null);
        return retorno;
    }

    public int enviar(List<Marcacao> marcacoes, Context ctx) {
        int retorno = 5;
        if (Util.estaConectado(ctx)) {
            List<ParametrosSOAP> parametros = new ArrayList<>();
            Compactador compactador = new Compactador();
            String serializable = compactador.serializeList(new ArrayList<Object>(marcacoes));
            ParametrosSOAP p1 = new ParametrosSOAP("marcacoes", serializable);
            parametros.add(p1);
            Servidor servidor = new Servidor(ctx);
            String resposta = servidor.comunicar("InserirMarcacoesService", "inserirMarcacoes" +
                    "", parametros);
            marcacoes = (List<Marcacao>) (List) compactador.deSerializeList(resposta);
            Log.i(MarcacaoControl.class.getName(), marcacoes.size() + "marcacoes");
            for (Marcacao marcacao : marcacoes) {
                MarcacaoControl mControl = new MarcacaoControl(ctx);
                long ret = mControl.alterar(marcacao);
                Log.i(MarcacaoControl.class.getName(), ret + "");
                retorno = (int) ret;
            }
        } else {
            retorno = 2;
        }
        return retorno;
    }

    public boolean existe(Marcacao marcacao) {
        boolean retorno = false;
        db = criarBanco.getReadableDatabase();
        String data = Util.formataDataBanco(marcacao.getData());
        String hora = Util.formataHoraBanco(marcacao.getHora());
        Cursor cursor = db.query("marcacao", new String[]{"id"}, "data = ? AND hora = ?", new String[]{data, hora}, null, null, null);
        while (cursor.moveToNext()) {
            retorno = true;
        }
        cursor.close();
        db.close();
        return retorno;
    }
}
