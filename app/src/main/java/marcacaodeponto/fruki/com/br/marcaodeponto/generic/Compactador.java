package marcacaodeponto.fruki.com.br.marcaodeponto.generic;


import org.apache.commons.io.IOUtils;
import org.apache.commons.net.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Compactador {

    private byte[] decompress(byte[] contentBytes) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        IOUtils.copy(new GZIPInputStream(new ByteArrayInputStream(contentBytes)), out);
        return out.toByteArray();
    }

    private byte[] decompress(String string) throws IOException {
        byte[] contentBytes = Base64.decodeBase64(string);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        IOUtils.copy(new GZIPInputStream(new ByteArrayInputStream(contentBytes)), out);
        return out.toByteArray();
    }

    private byte[] compress(byte[] content) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gzipOutputStream.write(content);
            gzipOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream.toByteArray();
    }

    public List<Object> deSerializeList(String obj) {
        List<Object> lista = null;
        try {
            byte[] b = decompress(obj);
            ObjectInputStream in = null;
            byte[] bytes = b;
            in = new ObjectInputStream(new ByteArrayInputStream(bytes));
            lista = (List<Object>) in.readObject();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Object deSerializeObject(String obj) {
        Object object = null;
        try {
            byte[] b = decompress(obj);
            ObjectInputStream in = null;
            byte[] bytes = b;
            in = new ObjectInputStream(new ByteArrayInputStream(bytes));
            object = (Object) in.readObject();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return object;
    }

    public String serializeObject(Object object) {
        String result = "";
        byte[] buf = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(object);
            out.close();
            buf = bos.toByteArray();
            result = Base64.encodeBase64String(compress(buf));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String serializeList(List<Object> list) {
        String result = "";
        byte[] buf = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(list);
            out.close();
            buf = bos.toByteArray();
            result = Base64.encodeBase64String(compress(buf));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
